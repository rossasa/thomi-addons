Odoo 8 tools by [odoo-solution.ch](http://www.odoo-solution.ch)
=============

account_followup_custom_master (not installable)
-------------

**Swiss compatible followup/overdue module template**

Default follow up workflow will be enhanced with functions to manually change the followup level for single move lines.
In followup level configuration, there is a additional field for fees, if you want add a fee for a specific followup level. Remember, if customer pays open invoices and the followup fee, you have to manually create a move line for the fee itself.
This module is only a template and is not installable...

***

account_invoice_rounding (not installable)
-------------

**DEPRECATED**

***

account_report_multicompany_fix
-------------

**Fix for github bug 6447: [8.0 Computed fields in multicompany environment produce "Access Error"](https://github.com/odoo/odoo/issues/6447)**

***

account_tax_codes_report_fix
-------------

**This enhancement allows you to print Tax code report from particular tax code.**

***

account_tax_fiscal_position_fix
-------------

**Fixes Odoo VAT handling, if taxes included and fiscal position used.**

***

account_voucher_payment_fix
-------------

**Fix for github bug 1892: [Difference Amount in "Register Payment" is not displayed properly](https://github.com/odoo/odoo/issues/1892)**

**Fix for lp bug 1202127: [Can't able to pay with write-off via register payment button.](https://bugs.launchpad.net/openobject-addons/+bug/1202127)**

**Fix for github bug 6251: [Sale/Purchase Receipt wrong period on journal change](https://github.com/odoo/odoo/issues/6251)**


* INFO: does fix problem of unbalanced move lines on customer/purchase receipts which contains taxes

***

hr_payslip_extension
-------------

**Swiss payroll customization**

Default Odoo payroll methods are enhanced with some Swiss specific functions.
Additional, Swiss specific salary rules are added.
A simple timesheet is used to easy capture working times of employee.

***

hr_payslip_extension_report
-------------

**Swiss payroll allowance and deduction overview report**

***

l10n_ch_account_vat_payment
-------------

**Account VAT based on invoice full and partial payments to create VAT report based on payments**

**Configuration**

This module needs a special VAT suspend account, where invoice taxes will be temporary saved after invoice validation. If invoice payment is registered, taxes will be moved from VAT suspend account to default VAT account.

The VAT suspend account does hold all open taxes where invoice isn't paid.

**Information**

In directory scripts, you will find a script (pau_script_v8) to update all invoices to fit this module. The script will cancel invoices (reconcile move lines, remove payment), validate and register payment, if invoice was paid before.

This module also contains a new VAT report, which is based on payments. You will find it at same place where the default VAT report is listed.

***

mail_custom
-------------

**Customized module for Mail**

Incoming, new messages from existing partners will be automatically added to partner history.
Odoo default method allows only to add messages to partner history if partner has replied to a message which was sent by Odoo.

***
