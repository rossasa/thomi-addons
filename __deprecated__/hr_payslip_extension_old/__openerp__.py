# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'HR Payslip Extension',
    'version': '1.0',
    'category': 'Human Resources',
    'sequence': 29,
    'summary': 'Payslip Enhancement',
    'description': """
Enhancement for Payslip
=======================

This enhancement allows you to enter specific payslip data per Employee and also enhances Salary Structure.
    """,
    'author': 'Tech-Receptives Solutions Pvt. Ltd.',
    'website': 'http://www.techreceptives.com',
    'images': [],
    'depends': [ 'hr_timesheet_sheet', 'hr_expense', 'hr_payroll'],
    'data': [
            'security/employee_group.xml',
            'security/ir.model.access.csv',
            'wizard/add_employee_timesheet_view.xml',
            'hr_my_timesheet_view.xml',
            'hr_employee_view.xml',
            'hr_payslip_view.xml',
            'hr_payroll_data.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
