# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "Account Invoice Rounding",
    "version": "1.0",
    "author": "Tech-Receptives Solutions Pvt. Ltd.",
    "category": "",
    "description": """
    Custom module For Rounding Factor in Account Invoice
    
    WARNING: Don't install this module along to account_voucher_payment_fix, it is already integreated!
    
    INFO: If you only want a correct running Register Payment process, just install account_voucher_payment_fix!
    """,
    "init_xml": [],
    "depends": ['web', 'base','product','account','account_voucher'],

    'data': [
            'account/account_view.xml',
            'rounding_factor/rounding_factor_view.xml',
            ],
            
    'installable': False,
    'application': True,
    "sequence": 0,
    'active': False,
}
