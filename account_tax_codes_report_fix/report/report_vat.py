# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.report import report_sxw
from openerp.addons.account.report.common_report_header import common_report_header
from openerp.addons.account.report.report_vat import tax_report


class tax_report_inherited(tax_report, report_sxw.rml_parse, common_report_header):

    def set_context(self, objects, data, ids, report_type=None):
        new_ids = ids
        if not data:
            company_id = self.pool['res.users'].browse(self.cr, self.uid, self.uid).company_id.id
            data = {
                'form': {
                    'based_on': 'invoices',
                    'company_id': company_id,
                    'display_detail': False,
                    'chart_tax_id': objects.parent_id.id
                }
            }
        return super(tax_report_inherited, self).set_context(objects, data, new_ids, report_type=report_type)


class report_vat(osv.AbstractModel):
    _name = 'report.account.report_vat'
    _inherit = 'report.abstract_report'
    _template = 'account.report_vat'
    _wrapped_report_class = tax_report_inherited

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
