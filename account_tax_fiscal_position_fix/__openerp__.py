# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': "Customization in Odoo VAT handling",
    'version': '0.1',
    'category': 'Generic Modules/Account',
    'description': """
Odoo VAT handling, if included and fiscal position used.
""",
    "author": "Tech-Receptives Solutions Pvt. Ltd.",
    'website': 'https://www.techreceptives.com/',
    'license': 'AGPL-3',
    "depends" : ['purchase', 'account_accountant'],
    "init_xml" : [],
    'data': [
        ],
    "demo_xml" : [],
    "installable": True,
    "application": True,
    'auto_install': False
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

