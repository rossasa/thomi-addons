# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd. & Win-Soft - Web Solution
#    (C) Copyright 2013-TODAY Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Account Voucher Payment',
    'version': '1.0',
    'category': 'Account',
    'description': """
        This module is used for under and over payment.
        On register payment, you can write-off under/over payment as usual and can also select the needed Tax Account.
        
        It installs also account_voucher_payment_fix, which does fix the Odoo issue about difference calculation for under payment.
    """,
    'author': 'Tech-Receptives Pvt. Ltd. & Win-Soft - Web Solution',
    'website': 'http://www.techreceptives.com',
    'images': [],
    'depends': ['account', 'account_voucher', 'account_voucher_payment_fix'],
    'data': [
            'inherit_account_vocher.xml',
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
