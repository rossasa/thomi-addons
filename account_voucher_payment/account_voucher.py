# -*- coding: utf-8 -*-
#/#############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2004-TODAY Tech-Receptives
#    (<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################
from openerp.osv import fields, osv


class account_voucher(osv.osv):

    """ Inherits the account.voucher class and methods
    """
    _inherit = 'account.voucher'

    _columns = {
        'tax_id': fields.many2one('account.tax', 'Tax'),
    }

    def writeoff_move_line_get(self, cr, uid, voucher_id, line_total, move_id, name, company_currency, current_currency, context=None):
        move_line = {}
        res = super(account_voucher, self).writeoff_move_line_get(
            cr, uid, voucher_id, line_total, move_id, name, company_currency, current_currency, context=context)
        voucher = self.browse(cr, uid, voucher_id)
        currency_obj = self.pool.get('res.currency')
        move_line_pool = self.pool.get('account.move.line')
        currency = voucher.currency_id or voucher.company_id.currency_id
        line_total = currency_obj.round(
            cr, uid, currency, line_total)
        sign = voucher.type == 'payment' and -1 or 1
        if line_total and voucher:
            tr = voucher.tax_id.amount
            cp = line_total / (tr + 1)
            tax = (cp * tr)
            total = cp + tax
            currency = voucher.currency_id or voucher.company_id.currency_id
            cp = currency_obj.round(
                cr, uid, currency, cp)
            tax = currency_obj.round(
                cr, uid, currency, tax)
            if cp and tax:
                cp_tax_amount = cp
                tax_tax_amount = tax
                res.update({
                    'name': voucher.comment,
                    'credit': cp > 0 and cp or 0.0,
                    'debit': cp < 0 and -cp or 0.0,
                    'tax_amount': cp_tax_amount or 0.0,
                    'tax_code_id': voucher.tax_id.ref_base_code_id.id,
                })
                account_id = voucher.tax_id.account_collected_id.id
                move_line = {
                    'name': voucher.comment,
                    'account_id': res['account_id'],
                    'move_id': res['move_id'],
                    'partner_id': voucher.partner_id.id,
                    'date': voucher.date,
                    'credit': tax > 0 and tax or 0.0,
                    'debit': tax < 0 and -tax or 0.0,
                    'tax_code_id': voucher.tax_id.ref_base_code_id.id,
                    'tax_amount': tax_tax_amount or 0.0,
                    'account_id': account_id,
                    'amount_currency': company_currency <> current_currency and (sign * -1 * voucher.writeoff_amount) or 0.0,
                    'currency_id': company_currency <> current_currency and current_currency or False,
                    'analytic_account_id': voucher.analytic_id and voucher.analytic_id.id or False,
                }
                context.update({'currency': currency})
                move_line_pool.create(cr, uid, move_line, context)
        return res

    def proforma_voucher(self, cr, uid, ids, context=None):
        self.action_move_line_create(cr, uid, ids, context=context)
        return True
