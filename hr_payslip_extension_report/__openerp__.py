# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'HR Consolidated Payslip',
    'version': '1.0',
    'category': 'Human Resources',
    'sequence': 01,
    'summary': 'Expenses Validation, Invoicing Report and Consolidated payslip',
    'description': """
Manage expenses by Employees
============================

This application allows you to manage your employees' daily expenses. It gives you access to your employees’ fee notes and give you the right to complete and validate or refuse the notes. After validation it creates an invoice for the employee.
Employee can encode their own expenses and the validation flow puts it automatically in the accounting after validation by managers
.This process through module generate consolidate payslip.


The whole flow is implemented as:
---------------------------------
* Draft expense
* Confirmation of the sheet by the employee
* Validation by his manager
* Validation by the accountant and receipt creation

This module also uses analytic accounting and is compatible with the invoice on timesheet module so that you are able to automatically re-invoice your customers' expenses if your work by project.
    """,
    'author': 'Tech-Receptives Solutions Pvt. Ltd.',
    'website': 'http://www.techreceptives.com',
    'images': [],
    'depends': [ 'hr_payslip_extension'],
    'data': [
            'report/report_paperformat.xml',
            'report/report_menu.xml',
            'view/report_hr_payroll_view.xml',
            'wizard/custom_payslip_wizard_view.xml',
            'wizard/all_employee_payslip_wizard_view.xml',
            'view/all_employee_hr_payroll_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
