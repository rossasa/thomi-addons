# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2011-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
        "name" : "Customization Vat Calculation",
        "version" : "1.0",
        'author': 'Tech-Receptives Solutions Pvt. Ltd.',
        'website': 'http://www.tech-receptives.com',
        "category" : "Enterprise Specific Modules",
        "description": """Vat Calculation Cash Basis""",
        "depends" : ['base', 'account', 'account_voucher'],
        "data" : [
            'account_view.xml',
            'report/report_paperformat.xml',
            'report/report_menu.xml',
            'wizard/account_vat_view.xml',
            'wizard/account_vat_special_view.xml',
            'view/account_vat_view.xml',
            'view/account_vat_special_view.xml',
        ],
        "installable": True,
        'application': True,
        'auto_install': False,

}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: