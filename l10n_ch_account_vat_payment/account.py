# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2011-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import itertools
from lxml import etree
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.tools import float_compare
import openerp.addons.decimal_precision as dp
from openerp.osv import osv,fields
from openerp.tools.translate import _
import time
from openerp import tools
from openerp import SUPERUSER_ID


class res_company(osv.osv):
    
    _inherit = "res.company"
    
    _columns = {
        'vat_account': fields.many2one('account.account','Vat Suspense Account'),
    }

class account_tax(osv.osv):
    _inherit = 'account.tax'
    _description = 'Tax'
        
     
    _columns = {
                'manual_amount_tax': fields.boolean('Manual Tax Amount'),
                }
    
class account_voucher(osv.osv):
    
    _inherit = "account.voucher"
    
    def move_button_cancel(self, cr, uid, ids, context=None):
        move_pool = self.pool.get('account.move')
        journal_pool = self.pool.get('account.journal')
        
        for line in move_pool.browse(cr, uid, ids, context=context):
            if not line.journal_id.update_posted:
                journal_pool.write(cr,uid,[line.journal_id.id],{'update_posted':True},context)
        if ids:
            cr.execute('UPDATE account_move '\
                       'SET state=%s '\
                       'WHERE id IN %s', ('draft', tuple(ids),))
            move_pool.invalidate_cache(cr, uid, context=context)
        return True
    
    def move_cancel_voucher(self, cr, uid, ids, context=None):
        reconcile_pool = self.pool.get('account.move.reconcile')
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        for voucher in self.browse(cr, uid, ids, context=context):
            # refresh to make sure you don't unlink an already removed move
            voucher.refresh()
            for line in voucher.move_ids:
                # refresh to make sure you don't unreconcile an already unreconciled entry
                line.refresh()
                if line.reconcile_id:
                    move_lines = [move_line.id for move_line in line.reconcile_id.line_id]
                    move_lines.remove(line.id)
                    reconcile_pool.unlink(cr, uid, [line.reconcile_id.id])
                    if len(move_lines) >= 2:
                        move_line_pool.reconcile_partial(cr, uid, move_lines, 'auto',context=context)
            if voucher.move_id:
                self.move_button_cancel(cr, uid, [voucher.move_id.id])
                move_pool.unlink(cr, uid, [voucher.move_id.id])
        res = {
            'state':'cancel',
            'move_id':False,
        }
        self.write(cr, uid, ids, res)
        return True
    
    
    
    def update_cancel_receipt_payment(self, cr, uid, ids, context=None):
        self.move_cancel_voucher(cr,uid,ids,context)
        self.action_cancel_draft(cr,uid,ids,context)
        self.proforma_voucher(cr,uid,ids,context)
        return True
    
    
    def _paid_amount_in_company_currency(self, cr, uid, ids, name, args, context=None):
        if context is None:
            context = {}
        res = {}
        ctx = context.copy()
        for v in self.browse(cr, uid, ids, context=context):
            ctx.update({'date': v.date})
            #make a new call to browse in order to have the right date in the context, to get the right currency rate
            voucher = self.browse(cr, uid, v.id, context=ctx)
            ctx.update({
              'voucher_special_currency': voucher.payment_rate_currency_id and voucher.payment_rate_currency_id.id or False,
              'voucher_special_currency_rate': voucher.currency_id.rate * voucher.payment_rate,})
            res[voucher.id] = voucher.amount  
#             self.pool.get('res.currency').compute(cr, uid, voucher.currency_id.id, voucher.company_id.currency_id.id, voucher.amount, context=ctx)
        return res
    
    _columns = {
            'paid_amount_in_company_currency': fields.function(_paid_amount_in_company_currency, string='Paid Amount in Company Currency', type='float', readonly=True),    
            'manual_tax':fields.boolean("Manual Tax"),    
            }
    
    _defaults = {
                 'manual_tax': False
                 }
    
    
    def action_move_line_create(self, cr, uid, ids, context=None):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        '''
        if context is None:
            context = {}
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        for voucher in self.browse(cr, uid, ids, context=context):
            if voucher.tax_id:
                self.compute_tax(cr, uid, [voucher.id], context)
                voucher = self.browse(cr, uid, voucher.id)
            local_context = dict(context, force_company=voucher.journal_id.company_id.id)
            if voucher.move_id:
                continue
            company_currency = self._get_company_currency(cr, uid, voucher.id, context)
            current_currency = self._get_current_currency(cr, uid, voucher.id, context)
            # we select the context to use accordingly if it's a multicurrency case or not
            context = self._sel_context(cr, uid, voucher.id, context)
            # But for the operations made by _convert_amount, we always need to give the date in the context
            ctx = context.copy()
            ctx.update({'date': voucher.date})
            # Create the account move record.
            move_id = move_pool.create(cr, uid, self.account_move_get(cr, uid, voucher.id, context=context), context=context)
            # Get the name of the account_move just created
            name = move_pool.browse(cr, uid, move_id, context=context).name
            # Create the first line of the voucher
            move_line_id = move_line_pool.create(cr, uid, self.first_move_line_get(cr,uid,voucher.id, move_id, company_currency, current_currency, local_context), local_context)
            move_line_brw = move_line_pool.browse(cr, uid, move_line_id, context=context)
            line_total = move_line_brw.debit - move_line_brw.credit
            rec_list_ids = []
            if voucher.tax_id and not voucher.tax_id.price_include:
                if voucher.type == 'sale':
                    line_total = line_total - self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
                elif voucher.type == 'purchase':
                    line_total = line_total + self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            if voucher.tax_id and voucher.tax_id.price_include and voucher.tax_id.manual_amount_tax:
                if voucher.type == 'sale':
                    line_total = line_total - self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
                elif voucher.type == 'purchase':
                    line_total = line_total + self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            # Create one move line per voucher line where amount is not 0.0
            line_total, rec_list_ids = self.voucher_move_line_create(cr, uid, voucher.id, line_total, move_id, company_currency, current_currency, context)
            # Create the writeoff line if needed
            ml_writeoff = self.writeoff_move_line_get(cr, uid, voucher.id, line_total, move_id, name, company_currency, current_currency, local_context)
            if ml_writeoff:
                move_line_pool.create(cr, uid, ml_writeoff, local_context)
            # We post the voucher.
            self.write(cr, uid, [voucher.id], {
                'move_id': move_id,
                'state': 'posted',
                'number': name,
            })
            if voucher.journal_id.entry_posted:
                move_pool.post(cr, uid, [move_id], context={})
            # We automatically reconcile the account move lines.
            reconcile = False
            for rec_ids in rec_list_ids:
                if len(rec_ids) >= 2:
                    reconcile = move_line_pool.reconcile_partial(cr, uid, rec_ids, writeoff_acc_id=voucher.writeoff_acc_id.id, writeoff_period_id=voucher.period_id.id, writeoff_journal_id=voucher.journal_id.id)
        return True
    
    
    def voucher_move_line_create(self, cr, uid, voucher_id, line_total, move_id, company_currency, current_currency, context=None):
        '''
        Create one account move line, on the given account move, per voucher line where amount is not 0.0.
        It returns Tuple with tot_line what is total of difference between debit and credit and
        a list of lists with ids to be reconciled with this format (total_deb_cred,list_of_lists).
 
        :param voucher_id: Voucher id what we are working with
        :param line_total: Amount of the first line, which correspond to the amount we should totally split among all voucher lines.
        :param move_id: Account move wher those lines will be joined.
        :param company_currency: id of currency of the company to which the voucher belong
        :param current_currency: id of currency of the voucher
        :return: Tuple build as (remaining amount not allocated on voucher lines, list of account_move_line created in this method)
        :rtype: tuple(float, list of int)
        '''
        if context is None:
            context = {}
        move_line_obj = self.pool.get('account.move.line')
        currency_obj = self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        tot_line = line_total
        rec_lst_ids = []
 
        
        search_ids = move_line_obj.search(cr,uid,[('move_id','=',move_id)],context)
        
        date = self.read(cr, uid, [voucher_id], ['date'], context=context)[0]['date']
        ctx = context.copy()
        ctx.update({'date': date})
        voucher = self.pool.get('account.voucher').browse(cr, uid, voucher_id, context=ctx)
        voucher_currency = voucher.journal_id.currency or voucher.company_id.currency_id
        ctx.update({
            'voucher_special_currency_rate': voucher_currency.rate * voucher.payment_rate ,
            'voucher_special_currency': voucher.payment_rate_currency_id and voucher.payment_rate_currency_id.id or False,})
        
        ctx.update({'tax_amount': voucher.tax_amount})
        if voucher.manual_tax:
            ctx.update({'proforma_voucher': True})
        prec = self.pool.get('decimal.precision').precision_get(cr, uid, 'Account')
        tax_lst = []
        for line in voucher.line_ids:
        #create one move line per voucher line where amount is not 0.0
        # AND (second part of the clause) only if the original move line was not having debit = credit = 0 (which is a legal value)
            if not line.amount and not (line.move_line_id and not float_compare(line.move_line_id.debit, line.move_line_id.credit, precision_digits=prec) and not float_compare(line.move_line_id.debit, 0.0, precision_digits=prec)):
                continue
            # convert the amount set on the voucher line into the currency of the voucher's company
            # this calls res_curreny.compute() with the right context, so that it will take either the rate on the voucher if it is relevant or will use the default behaviour
            amount = self._convert_amount(cr, uid, line.untax_amount or line.amount, voucher.id, context=ctx)
            # if the amount encoded in voucher is equal to the amount unreconciled, we need to compute the
            # currency rate difference
            if line.amount == line.amount_unreconciled:
                if not line.move_line_id:
                    raise osv.except_osv(_('Wrong voucher line'),_("The invoice you are willing to pay is not valid anymore."))
                sign = line.type =='dr' and -1 or 1
                currency_rate_difference = sign * (line.move_line_id.amount_residual - amount)
            else:
                currency_rate_difference = 0.0
            move_line = {
                'journal_id': voucher.journal_id.id,
                'period_id': voucher.period_id.id,
                'name': line.name or '/',
                'account_id': line.account_id.id,
                'move_id': move_id,
                'partner_id': voucher.partner_id.id,
                'currency_id': line.move_line_id and (company_currency <> line.move_line_id.currency_id.id and line.move_line_id.currency_id.id) or False,
                'analytic_account_id': line.account_analytic_id and line.account_analytic_id.id or False,
                'quantity': 1,
                'credit': 0.0,
                'debit': 0.0,
                'date': voucher.date,
#                 'tax_code_id': 13
            }
            tax_move_line_ids = self.pool.get('account.move.line').search(cr, uid, [('move_id','=',line.move_line_id.move_id.id),'|',('credit','=',line.move_line_id.invoice.amount_untaxed),('debit','=',line.move_line_id.invoice.amount_untaxed)])
            if tax_move_line_ids:
                tax_move_line_browse = self.pool.get('account.move.line').browse(cr, uid, tax_move_line_ids[0], context=context)
                move_line.update({'tax_code_id': tax_move_line_browse.tax_code_id and tax_move_line_browse.tax_code_id.id or False})
            if amount < 0:
                amount = -amount
                if line.type == 'dr':
                    line.type = 'cr'
                else:
                    line.type = 'dr'
 
            if (line.type=='dr'):
                tot_line += amount
                move_line['debit'] = amount
            else:
                tot_line -= amount
                move_line['credit'] = amount
            if voucher.tax_id and voucher.type in ('sale', 'purchase'):
                if voucher.manual_tax:
                    if voucher.tax_id not in tax_lst:
                        move_line.update({
                            'account_tax_id': voucher.tax_id.id,
                        })
                        tax_lst.append(voucher.tax_id)
                else:
                    move_line.update({
                            'account_tax_id': voucher.tax_id.id,
                        })
            if move_line.get('account_tax_id', False):
                tax_data = tax_obj.browse(cr, uid, [move_line['account_tax_id']], context=context)[0]
#                 move_line.update({'tax_code_id': tax_data.base_code_id and tax_data.base_code_id.id or False})
                if not (tax_data.base_code_id and tax_data.tax_code_id):
                    raise osv.except_osv(_('No Account Base Code and Account Tax Code!'),_("You have to configure account base code and account tax code on the '%s' tax!") % (tax_data.name))
 
            # compute the amount in foreign currency
            foreign_currency_diff = 0.0
            amount_currency = False
            if line.move_line_id:
                # We want to set it on the account move line as soon as the original line had a foreign currency
                if line.move_line_id.currency_id and line.move_line_id.currency_id.id != company_currency:
                    # we compute the amount in that foreign currency.
                    if line.move_line_id.currency_id.id == current_currency:
                        # if the voucher and the voucher line share the same currency, there is no computation to do
                        sign = (move_line['debit'] - move_line['credit']) < 0 and -1 or 1
                        amount_currency = sign * (line.amount)
                    else:
                        # if the rate is specified on the voucher, it will be used thanks to the special keys in the context
                        # otherwise we use the rates of the system
                        amount_currency = currency_obj.compute(cr, uid, company_currency, line.move_line_id.currency_id.id, move_line['debit']-move_line['credit'], context=ctx)
                if line.amount == line.amount_unreconciled:
                    foreign_currency_diff = line.move_line_id.amount_residual_currency - abs(amount_currency)
 
            move_line['amount_currency'] = amount_currency
            if move_line.get('account_tax_id',False):
                ctx.update({'tax_amount': voucher.tax_amount})
            voucher_line = move_line_obj.create(cr, uid, move_line,context=ctx)
            rec_ids = [voucher_line, line.move_line_id.id]
            new_rec_ids = []
            
            if line.move_line_id.invoice.amount_total > 0.0 and abs(line_total) > 0: 
                calculated_amount = abs(line_total)/line.move_line_id.invoice.amount_total
            else:
                calculated_amount = 0.0
                
            if line.move_line_id.invoice and line.move_line_id.invoice.amount_tax:
                for t_line in line.move_line_id.invoice.tax_line:
                    move_line1 = {
                            'journal_id': voucher.journal_id.id,
                            'period_id': voucher.period_id.id,
                            'name': t_line.name or '/',
                            'account_id': t_line.account_id.id,
                            'move_id': move_id,
                            'partner_id': voucher.partner_id.id,
                            'currency_id': line.move_line_id and (company_currency <> line.move_line_id.currency_id.id and line.move_line_id.currency_id.id) or False,
                            'analytic_account_id': line.account_analytic_id and line.account_analytic_id.id or False,
                            'tax_code_id': t_line.tax_code_id and t_line.tax_code_id.id or False,
                            'base_tax_code_id': t_line.base_code_id and t_line.base_code_id.id or False,
                            'base_amount': 0.0,
                            'quantity': 1,
                            'credit': 0.0,
                            'debit': 0.0,
                            'date': voucher.date,
                        }
                    move_line2 = {
                        'journal_id': voucher.journal_id.id,
                        'period_id': voucher.period_id.id,
                        'name': t_line.name or '/',
                        'account_id': voucher.company_id.vat_account.id,
                        'move_id': move_id,
#                         'tax_code_id': t_line.tax_code_id and t_line.tax_code_id.id or False,
#                         'base_tax_code_id': t_line.base_code_id and t_line.base_code_id.id or False,
#                         'base_amount':t_line.amount or 0.0,
                        'partner_id': voucher.partner_id.id,
                        'currency_id': line.move_line_id and (company_currency <> line.move_line_id.currency_id.id and line.move_line_id.currency_id.id) or False,
                        'analytic_account_id': line.account_analytic_id and line.account_analytic_id.id or False,
                        'quantity': 1,
                        'credit': 0.0,
                        'debit': 0.0,
                        'date': voucher.date,
                    }
                    if t_line.amount:
                        t_line_amount = abs(t_line.amount/t_line.base_amount)
                        l_amount = abs(line.amount)
                        t_d_amount = 1+t_line_amount
                        if t_line.invoice_id.type in ['out_invoice', 'in_refund'] :
                            if t_line.amount > 0.0:
                                move_line1.update({'credit': (t_line.amount * calculated_amount),'base_amount':t_line.base * calculated_amount})
                                move_line2.update({'debit': (t_line.amount * calculated_amount)})
                            else:
                                move_line2.update({'credit': (t_line.amount * calculated_amount)})
                                move_line1.update({'debit': (t_line.amount * calculated_amount),'base_amount':t_line.base * calculated_amount * -1}) 
                            new_rec_ids.append(move_line_obj.create(cr, uid, move_line1))
                            new_rec_ids.append(move_line_obj.create(cr, uid, move_line2))
                        elif t_line.invoice_id.type in ['in_invoice', 'out_refund']:
                            if t_line.amount > 0.0:
                                move_line2.update({'credit': (t_line.amount * calculated_amount)})
                                move_line1.update({'debit': (t_line.amount * calculated_amount),'base_amount':t_line.base * calculated_amount * -1})
                            else:
                                move_line1.update({'credit': (t_line.amount * calculated_amount),'base_amount':t_line.base * calculated_amount})
                                move_line2.update({'debit': (t_line.amount * calculated_amount)})
                            new_rec_ids.append(move_line_obj.create(cr, uid, move_line1))
                            new_rec_ids.append(move_line_obj.create(cr, uid, move_line2))
                            
            if not currency_obj.is_zero(cr, uid, voucher.company_id.currency_id, currency_rate_difference):
                # Change difference entry in company currency
                exch_lines = self._get_exchange_lines(cr, uid, line, move_id, currency_rate_difference, company_currency, current_currency, context=context)
                new_id = move_line_obj.create(cr, uid, exch_lines[0],context)
                test = move_line_obj.create(cr, uid, exch_lines[1], context)
                rec_ids.append(new_id)
            if line.move_line_id and line.move_line_id.currency_id and not currency_obj.is_zero(cr, uid, line.move_line_id.currency_id, foreign_currency_diff):
                # Change difference entry in voucher currency
                move_line_foreign_currency = {
                    'journal_id': line.voucher_id.journal_id.id,
                    'period_id': line.voucher_id.period_id.id,
                    'name': _('change')+': '+(line.name or '/'),
                    'account_id': line.account_id.id,
                    'move_id': move_id,
                    'partner_id': line.voucher_id.partner_id.id,
                    'currency_id': line.move_line_id.currency_id.id,
                    'amount_currency': -1 * foreign_currency_diff,
                    'quantity': 1,
                    'credit': 0.0,
                    'debit': 0.0,
                    'date': line.voucher_id.date,
                }
                new_id = move_line_obj.create(cr, uid, move_line_foreign_currency, context=context)
                rec_ids.append(new_id)
            if line.move_line_id.id:
                rec_lst_ids.append(rec_ids)
        return (tot_line, rec_lst_ids)
   

    def onchange_price(self, cr, uid, ids, line_ids, tax_id, partner_id=False, context=None):
        context = context or {}
        res = super(account_voucher,self).onchange_price(cr, uid, ids, line_ids, tax_id, partner_id=False, context=context)
        tax = self.pool.get('account.tax').browse(cr, uid, tax_id, context=context)
        if tax.manual_amount_tax:
            res['value'].update({'manual_tax': True,})
        else:
            res['value'].update({'manual_tax': False,
                                 })
        return  res
    
    def compute_tax(self, cr, uid, ids, context=None):
        tax_pool = self.pool.get('account.tax')
        partner_pool = self.pool.get('res.partner')
        position_pool = self.pool.get('account.fiscal.position')
        voucher_line_pool = self.pool.get('account.voucher.line')
        voucher_pool = self.pool.get('account.voucher')
        if context is None: context = {}
   
        for voucher in voucher_pool.browse(cr, uid, ids, context=context):
            voucher_amount = 0.0
            for line in voucher.line_ids:
                voucher_amount += line.untax_amount or line.amount
                line.amount = line.untax_amount or line.amount
                voucher_line_pool.write(cr, uid, [line.id], {'amount':line.amount, 'untax_amount':line.untax_amount})
   
            if not voucher.tax_id:
                self.write(cr, uid, [voucher.id], {'amount':voucher_amount, 'tax_amount':0.0})
                continue
   
            tax = [tax_pool.browse(cr, uid, voucher.tax_id.id, context=context)]
            partner = partner_pool.browse(cr, uid, voucher.partner_id.id, context=context) or False
            taxes = position_pool.map_tax(cr, uid, partner and partner.property_account_position or False, tax)
            tax = tax_pool.browse(cr, uid, taxes, context=context)
            total = voucher_amount
            total_tax = 0.0
            if not tax[0].price_include:
                for line in voucher.line_ids:
                    if voucher.manual_tax:
                        for tax_line in tax_pool.compute_all(cr, uid, tax, voucher.tax_amount, 1).get('taxes', []):
                            total_tax = voucher.tax_amount
                    else:
                        for tax_line in tax_pool.compute_all(cr, uid, tax, line.amount, 1).get('taxes', []):
                            total_tax += tax_line.get('amount', 0.0)
                total += total_tax
            
            elif tax[0].price_include and tax[0].manual_amount_tax:
                for line in voucher.line_ids:
                    if voucher.manual_tax:
                        for tax_line in tax_pool.compute_all(cr, uid, tax, voucher.tax_amount, 1).get('taxes', []):
                            total_tax = voucher.tax_amount
                    else:
                        for tax_line in tax_pool.compute_all(cr, uid, tax, line.amount, 1).get('taxes', []):
                            total_tax += tax_line.get('amount', 0.0)
                total += total_tax
                   
            else:
                for line in voucher.line_ids:
                    line_total = 0.0
                    line_tax = 0.0
                    for tax_line in tax_pool.compute_all(cr, uid, tax, line.untax_amount or line.amount, 1).get('taxes', []):
                        line_tax += tax_line.get('amount', 0.0)
                        line_total += tax_line.get('price_unit')
                    total_tax += line_tax
                    untax_amount = line.untax_amount or line.amount
                    voucher_line_pool.write(cr, uid, [line.id], {'amount':line_total, 'untax_amount':untax_amount})
            self.write(cr, uid, [voucher.id], {'amount':total, 'tax_amount':total_tax})
        return True
   
class account_move_line(osv.osv):
    _inherit = 'account.move.line'
    
    _columns = {
            'base_tax_code_id': fields.many2one('account.tax.code', 'Base Tax Account'),
            'base_amount': fields.float('Base Amount')
        }
    
    
    def create(self, cr, uid, vals, context=None, check=True):
#         if vals.get('name') and vals['name'] == '/ Tax 15.00%':
#             stop
        account_obj = self.pool.get('account.account')
        tax_obj = self.pool.get('account.tax')
        move_obj = self.pool.get('account.move')
        cur_obj = self.pool.get('res.currency')
        journal_obj = self.pool.get('account.journal')
        context = dict(context or {})
        if vals.get('move_id', False):
            move = self.pool.get('account.move').browse(cr, uid, vals['move_id'], context=context)
            if move.company_id:
                vals['company_id'] = move.company_id.id
            if move.date and not vals.get('date'):
                vals['date'] = move.date
        if ('account_id' in vals) and not account_obj.read(cr, uid, [vals['account_id']], ['active'])[0]['active']:
            raise osv.except_osv(_('Bad Account!'), _('You cannot use an inactive account.'))
        if 'journal_id' in vals and vals['journal_id']:
            context['journal_id'] = vals['journal_id']
        if 'period_id' in vals and vals['period_id']:
            context['period_id'] = vals['period_id']
        if ('journal_id' not in context) and ('move_id' in vals) and vals['move_id']:
            m = move_obj.browse(cr, uid, vals['move_id'])
            context['journal_id'] = m.journal_id.id
            context['period_id'] = m.period_id.id
        #we need to treat the case where a value is given in the context for period_id as a string
        if 'period_id' in context and not isinstance(context.get('period_id', ''), (int, long)):
            period_candidate_ids = self.pool.get('account.period').name_search(cr, uid, name=context.get('period_id',''))
            if len(period_candidate_ids) != 1:
                raise osv.except_osv(_('Error!'), _('No period found or more than one period found for the given date.'))
            context['period_id'] = period_candidate_ids[0][0]
        if not context.get('journal_id', False) and context.get('search_default_journal_id', False):
            context['journal_id'] = context.get('search_default_journal_id')
        self._update_journal_check(cr, uid, context['journal_id'], context['period_id'], context)
        move_id = vals.get('move_id', False)
        journal = journal_obj.browse(cr, uid, context['journal_id'], context=context)
        vals['journal_id'] = vals.get('journal_id') or context.get('journal_id')
        vals['period_id'] = vals.get('period_id') or context.get('period_id')
        vals['date'] = vals.get('date') or context.get('date')
        if not move_id:
            if journal.centralisation:
                #Check for centralisation
                res = self._check_moves(cr, uid, context)
                if res:
                    vals['move_id'] = res[0]
            if not vals.get('move_id', False):
                if journal.sequence_id:
                    #name = self.pool.get('ir.sequence').next_by_id(cr, uid, journal.sequence_id.id)
                    v = {
                        'date': vals.get('date', time.strftime('%Y-%m-%d')),
                        'period_id': context['period_id'],
                        'journal_id': context['journal_id']
                    }
                    if vals.get('ref', ''):
                        v.update({'ref': vals['ref']})
                    move_id = move_obj.create(cr, uid, v, context)
                    vals['move_id'] = move_id
                else:
                    raise osv.except_osv(_('No Piece Number!'), _('Cannot create an automatic sequence for this piece.\nPut a sequence in the journal definition for automatic numbering or create a sequence manually for this piece.'))
        ok = not (journal.type_control_ids or journal.account_control_ids)
        if ('account_id' in vals):
            account = account_obj.browse(cr, uid, vals['account_id'], context=context)
            if journal.type_control_ids:
                type = account.user_type
                for t in journal.type_control_ids:
                    if type.code == t.code:
                        ok = True
                        break
            if journal.account_control_ids and not ok:
                for a in journal.account_control_ids:
                    if a.id == vals['account_id']:
                        ok = True
                        break
            # Automatically convert in the account's secondary currency if there is one and
            # the provided values were not already multi-currency
            if account.currency_id and 'amount_currency' not in vals and account.currency_id.id != account.company_id.currency_id.id:
                vals['currency_id'] = account.currency_id.id
                ctx = {}
                if 'date' in vals:
                    ctx['date'] = vals['date']
                vals['amount_currency'] = cur_obj.compute(cr, uid, account.company_id.currency_id.id,
                    account.currency_id.id, vals.get('debit', 0.0)-vals.get('credit', 0.0), context=ctx)
        if not ok:
            raise osv.except_osv(_('Bad Account!'), _('You cannot use this general account in this journal, check the tab \'Entry Controls\' on the related journal.'))
        
        if vals.get('base_tax_code_id',False) and context.get('proforma_voucher', False):
#             if context.get('invoice', False):
            tax_amount = context.get('tax_amount', 0.0)
            vals.update({'tax_amount': tax_amount,
                         'credit': tax_amount<0 and -tax_amount or 0.0,
                    'debit': tax_amount>0 and tax_amount or 0.0,})
        result = super(osv.osv, self).create(cr, uid, vals, context=context)
        # CREATE Taxes
        if vals.get('account_tax_id', False):
            tax_id = tax_obj.browse(cr, uid, vals['account_tax_id'])
            total = vals['debit'] - vals['credit']
            base_code = 'base_code_id'
            tax_code = 'tax_code_id'
            account_id = 'account_collected_id'
            base_sign = 'base_sign'
            tax_sign = 'tax_sign'
            if journal.type in ('purchase_refund', 'sale_refund') or (journal.type in ('cash', 'bank') and total < 0):
                base_code = 'ref_base_code_id'
                tax_code = 'ref_tax_code_id'
                account_id = 'account_paid_id'
                base_sign = 'ref_base_sign'
                tax_sign = 'ref_tax_sign'
            base_adjusted = False
            
            for tax in tax_obj.compute_all(cr, uid, [tax_id], total, 1.00, force_excluded=False).get('taxes'):
                #create the base movement
                if base_adjusted == False:
                    base_adjusted = True
                    if tax_id.price_include and not tax_id.manual_amount_tax:
                        total = tax['price_unit']
                    newvals = {
                        'tax_code_id': tax[base_code],
                        'tax_amount': tax[base_sign] * abs(total),
                    }
                    if tax_id.price_include and not tax_id.manual_amount_tax:
                        if tax['price_unit'] < 0:
                            newvals['credit'] = abs(tax['price_unit'])
                        else:
                            newvals['debit'] = tax['price_unit']
                    self.write(cr, uid, [result], newvals, context=context)
                else:
                    data = {
                        'move_id': vals['move_id'],
                        'name': tools.ustr(vals['name'] or '') + ' ' + tools.ustr(tax['name'] or ''),
                        'date': vals['date'],
                        'partner_id': vals.get('partner_id', False),
                        'ref': vals.get('ref', False),
                        'statement_id': vals.get('statement_id', False),
                        'account_tax_id': False,
                        'tax_code_id': tax[base_code],
                        'tax_amount': tax[base_sign] * abs(total),
                        'account_id': vals['account_id'],
                        'credit': 0.0,
                        'debit': 0.0,
                    }
                    self.create(cr, uid, data, context)
                #create the Tax movement
                if not tax['amount'] and not tax[tax_code]:
                    continue
                
                if tax_id.price_include and not tax_id.manual_amount_tax:
                    base_amount = (vals['debit'] > 0.0 and total * -1) or (vals['credit'] > 0.0 and total * -1) or total 
                else:
                   base_amount = vals['debit'] > 0.0 and vals['debit'] * -1 or vals['credit'] 
                   
                data = {
                    'move_id': vals['move_id'],
                    'name': tools.ustr(vals['name'] or '') + ' ' + tools.ustr(tax['name'] or ''),
                    'date': vals['date'],
                    'partner_id': vals.get('partner_id',False),
                    'ref': vals.get('ref',False),
                    'statement_id': vals.get('statement_id', False),
                    'account_tax_id': False,
                    'tax_code_id': tax[tax_code],
                    'tax_amount': tax[tax_sign] * abs(tax['amount']),
                    'account_id': tax[account_id] or vals['account_id'],
                    'credit': tax['amount']<0 and -tax['amount'] or 0.0,
                    'debit': tax['amount']>0 and tax['amount'] or 0.0,
                    'base_tax_code_id': tax[base_code],
                    'base_amount': base_amount
                }
                self.create(cr, uid, data, context)
            del vals['account_tax_id']

        recompute = journal.env.recompute and context.get('recompute', True)
        if check and not context.get('novalidate') and (recompute or journal.entry_posted):
            tmp = move_obj.validate(cr, uid, [vals['move_id']], context)
            if journal.entry_posted and tmp:
                move_obj.button_validate(cr,uid, [vals['move_id']], context)
        return result
    
    
    
#     def create(self, cr, uid, vals,context=None,check=True):
#         context = dict(context or {})
#         account_obj = self.pool.get('account.account')
#         tax_obj = self.pool.get('account.tax')
#         move_obj = self.pool.get('account.move')
#         cur_obj = self.pool.get('res.currency')
#         journal_obj = self.pool.get('account.journal')
#         if 'journal_id' in vals and vals['journal_id']:
#             context['journal_id'] = vals['journal_id']
#         journal = journal_obj.browse(cr, uid, context['journal_id'], context=context)
#         result = super(account_move_line, self).create(cr, uid, vals, context=context)
#         # CREATE Taxes
#         stop
#         if vals.get('account_tax_id', False):
#             tax_id = tax_obj.browse(cr, uid, vals['account_tax_id'])
#             total = vals['debit'] - vals['credit']
#             base_code = 'base_code_id'
#             tax_code = 'tax_code_id'
#             account_id = 'account_collected_id'
#             base_sign = 'base_sign'
#             tax_sign = 'tax_sign'
#             if journal.type in ('purchase_refund', 'sale_refund') or (journal.type in ('cash', 'bank') and total < 0):
#                 base_code = 'ref_base_code_id'
#                 tax_code = 'ref_tax_code_id'
#                 account_id = 'account_paid_id'
#                 base_sign = 'ref_base_sign'
#                 tax_sign = 'ref_tax_sign'
#             base_adjusted = False
#             for tax in tax_obj.compute_all(cr, uid, [tax_id], total, 1.00, force_excluded=False).get('taxes'):
#                 #create the base movement
#                 if base_adjusted == False:
#                     base_adjusted = True
#                     if tax_id.price_include:
#                         total = tax['price_unit']
#                     newvals = {
#                         'tax_code_id': tax[base_code],
#                         'tax_amount': tax[base_sign] * abs(total),
#                     }
#                     if tax_id.price_include:
#                         if tax['price_unit'] < 0:
#                             newvals['credit'] = abs(tax['price_unit'])
#                         else:
#                             newvals['debit'] = tax['price_unit']
#                     self.write(cr, uid, [result], newvals, context=context)
#                 else:
#                     data = {
#                         'move_id': vals['move_id'],
#                         'name': tools.ustr(vals['name'] or '') + ' ' + tools.ustr(tax['name'] or ''),
#                         'date': vals['date'],
#                         'partner_id': vals.get('partner_id', False),
#                         'ref': vals.get('ref', False),
#                         'statement_id': vals.get('statement_id', False),
#                         'account_tax_id': False,
#                         'tax_code_id': tax[base_code],
#                         'tax_amount': tax[base_sign] * abs(total),
#                         'account_id': vals['account_id'],
#                         'credit': 0.0,
#                         'debit': 0.0,
#                     }
#                     self.create(cr, uid, data, context)
#                 #create the Tax movement
#                 if not tax['amount'] and not tax[tax_code]:
#                     continue
#                 data = {
#                     'move_id': vals['move_id'],
#                     'name': tools.ustr(vals['name'] or '') + ' ' + tools.ustr(tax['name'] or ''),
#                     'date': vals['date'],
#                     'partner_id': vals.get('partner_id',False),
#                     'ref': vals.get('ref',False),
#                     'statement_id': vals.get('statement_id', False),
#                     'account_tax_id': False,
#                     'tax_code_id': tax[tax_code],
#                     'tax_amount': tax[tax_sign] * abs(tax['amount']),
#                     'account_id': tax[account_id] or vals['account_id'],
#                     'credit': tax['amount']<0 and -tax['amount'] or 0.0,
#                     'debit': tax['amount']>0 and tax['amount'] or 0.0,
#                     'base_tax_code_id': tax[base_code],
#                     'base_amount': vals['credit'] > 0.0 and vals['credit'] or vals['debit']
#                 }
#                 print "_________data_____________________",data
#                 stop
#                 self.create(cr, uid, data, context)
#             del vals['account_tax_id']
# 
#         recompute = journal.env.recompute and context.get('recompute', True)
#         if check and not context.get('novalidate') and (recompute or journal.entry_posted):
#             tmp = move_obj.validate(cr, uid, [vals['move_id']], context)
#             if journal.entry_posted and tmp:
#                 move_obj.button_validate(cr,uid, [vals['move_id']], context)
#         return result 
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:    