# -*- coding: utf-8 -*-
#/#############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2004-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################

import time
from openerp.osv import osv
from openerp.report import report_sxw
from openerp import pooler
from datetime import date,datetime
from openerp import netsvc
import itertools

class account_vat_reporting(report_sxw.rml_parse):
    _name="account.vat.reporting"
    
    def __init__(self, cr, uid, name, context={}):
        super(account_vat_reporting, self).__init__(cr, uid, name, context=context)
        total_list = []
        self.localcontext.update({
            'time': time,
            'datetime':datetime,
            'get_data':self.get_data,
            'get_start_period':self.get_start_period,
            'get_end_period':self.get_end_period,
            'get_total':self.get_total,
            'get_codes': self._get_codes,
        }) 
        self.total_list = total_list
    def get_total(self):
        return self.total_list
    
    def get_data(self,data,company_id=False,parent=False,level=0,context=None):
        
        res = []
        sum_lst = []
        base_sum_lst = []
        merged_lst = []
        tax_lst = []
        final_lst = []
        
        dict = new_dic1 = new_dic2 = {}
        amount_total = tax_amount_total = final_total = 0.0
        
        cr=self.cr
        uid=self.uid
        
        inv_obj = self.pool.get('account.invoice')
        voucher_obj = self.pool.get('account.voucher')
        period_obj = self.pool.get('account.period')
        
        period_from = period_obj.browse(cr,uid,data['form']['period_from'])
        period_to = period_obj.browse(cr,uid,data['form']['period_to'])
        
        period_list = period_obj.build_ctx_periods(self.cr, self.uid, data['form']['period_from'], data['form']['period_to'])
        
        getcode_res= self._get_codes(company_id, parent, level, context=context)
        addcode_res = self._add_codes(getcode_res, period_list, context=context)
        
        voucher_search_ids = voucher_obj.search(self.cr, self.uid, [('state','in',['posted'])],context)
        
        if voucher_search_ids:
            for voucher in voucher_obj.browse(cr,uid,voucher_search_ids):
               if voucher.date >= period_from.date_start and voucher.date <= period_to.date_stop and voucher.move_ids:
                    move_line = voucher.move_ids[0]
                    if move_line and move_line.move_id and move_line.move_id.state in ['posted']:
                        for line in move_line.move_id.line_id:
                            if line.tax_code_id and line.base_tax_code_id:
                                dict = {
                                    'tax_id': line.tax_code_id or False,
                                    'base_tax_id': line.base_tax_code_id or False,
                                    'tax_amount':(line.debit or 0.0)* -1 or (line.credit or 0.0),
                                    'base_tax_amount': line.invoice.type in ['in_invoice','out_refund'] and line.base_amount * -1  or line.base_amount,
                                    'credit': 0.0,
                                    'debit': 0.0,    
                            }
                                res.append(dict)
        if res:               
                for key, group in itertools.groupby(res, lambda item: item["tax_id"]):
                    tax_sum = amount_sum = total_sum = 0
                    for item in group:
#                         print "item_________________",item
#                          -----------------------------------------------------now Comment -------------
#                         if item['type'] in ['out_refund','in_invoice']:
#                             tax_sum -= item["tax_amount"]
# #                             amount_sum -= item["amount"] 
# #                             total_sum -= item["total"] 
#                         else:
                            tax_sum += item["tax_amount"]
#                             amount_sum += item["amount"] 
#                             total_sum += item["total"]
                                
                    tax_name = key.name
                    mydic = {'tax_name':tax_name,
                             'tax_sum':tax_sum,
#                              'amount_sum':amount_sum,
#                              'total_sum': total_sum
                            }
                    sum_lst.append(mydic)
#                     
                for base_key, base_group in itertools.groupby(res, lambda base_item: base_item["base_tax_id"]):
                    base_tax_name = ''
                    base_tax_sum = 0
                    for base_item in base_group:
#                         print "item_________________",item
                         
#                         if base_item['type'] in ['in_refund','out_invoice']:
#                             base_tax_sum -= base_item["base_tax_amount"]
#                             amount_sum -= item["amount"] 
#                             total_sum -= item["total"] 
#                         else:
                        base_tax_sum += base_item["base_tax_amount"]
#                             amount_sum += item["amount"] 
#                             total_sum += item["total"]
                    if base_key:         
                        base_tax_name = base_key.name
                    base_mydic = {'tax_name':base_tax_name,
                             'tax_sum':base_tax_sum,
#                              'amount_sum':amount_sum,
#                              'total_sum': total_sum
                            }
                    base_sum_lst.append(base_mydic)
                     
                sum_lst = self.arrange_list(sum_lst,base_sum_lst)
                 
#                 print "sunm______________lst________",sum_lst
                 
                if addcode_res:
                    for code in addcode_res:
                        flag=False
                        for dic in sum_lst:
                            if code[1]['name'] == dic['tax_name']:
                                new_dic1 = {
                                    'level': code[0],
                                    'tax_name': (code[1]['code'] or '')+" "+code[1]['name'],
                                    'tax_sum': dic['tax_sum'],
#                                     'amount_sum' : dic['amount_sum'],
#                                     'total_sum': dic['total_sum'],
                                    'credit':0.0,
                                    'debit':0.0,
                                    'id': code[1]['id'],
                                    'parent_id': code[1]['parent_id']
                                 }
                                flag = True
#                             
                            else:
                                new_dic2 = {
                                    'level': code[0],
                                    'tax_name': (code[1]['code'] or '')+" "+code[1]['name'],
                                    'tax_sum': 0.0,
                                    'credit':0.0,
                                    'debit':0.0,
#                                     'amount_sum' : 0.0,
#                                     'total_sum': 0.0,
                                    'id': code[1]['id'],
                                    'parent_id': code[1]['parent_id']
                                 }
                        if flag:
                            final_lst.append(new_dic1)
                        else:
                            final_lst.append(new_dic2)
        else:
                if addcode_res:
                    for code in addcode_res:
                        flag=False
                        new_dic2 = {
                            'level': code[0],
                            'tax_name': (code[1]['code'] or '')+" "+code[1]['name'],
                            'tax_sum': 0.0,
                            'credit':0.0,
                            'debit':0.0,
#                             'amount_sum' : 0.0,
#                             'total_sum': 0.0,
                            'id': code[1]['id'],
                            'parent_id': code[1]['parent_id']
                         }
                        final_lst.append(new_dic2)
                 
        if final_lst:
                for f_lst in reversed(final_lst):
                    for parent, child in itertools.groupby(final_lst, lambda item: item["parent_id"]):
                        for item in child:
                            if f_lst['id']==item['parent_id'].id:
                                f_lst['tax_sum'] += item['tax_sum']
#                                 f_lst['amount_sum'] += item['amount_sum']
#                                 f_lst['total_sum'] += item['total_sum']
                
        self.total_list.append({'final_total':final_total,'tax_amount_total':tax_amount_total,'amount_total':amount_total})
            
        return final_lst
    
    def arrange_list(self,sum_lst,base_sum_lst):
        final_lst = []
        for l in sum_lst:
            if final_lst and l['tax_name'] in [k['tax_name'] for k in final_lst]:
                for line in final_lst:
                    if line['tax_name'] == l['tax_name']:
#                         line['amount_sum'] += l['amount_sum']
# #                         line['total_sum'] += l['total_sum']
                        line['tax_sum'] += l['tax_sum']
            else:
                final_lst.append(l)
        
        for l in base_sum_lst:
            if final_lst and l['tax_name'] in [k['tax_name'] for k in final_lst]:
                for line in final_lst:
                    if line['tax_name'] == l['tax_name']:
#                         line['amount_sum'] += l['amount_sum']
#                         line['total_sum'] += l['total_sum']
                        line['tax_sum'] += l['tax_sum']
            else:
                final_lst.append(l)
        return final_lst
    
    def get_start_period(self, data):
        if data.get('form', False) and data['form'].get('period_from', False):
            return self.pool.get('account.period').browse(self.cr,self.uid,data['form']['period_from']).name
        return ''

    def get_end_period(self, data):
        if data.get('form', False) and data['form'].get('period_to', False):
            return self.pool.get('account.period').browse(self.cr, self.uid, data['form']['period_to']).name
        return ''
    
    def _get_codes(self, company_id, parent=False, level=0, context=None):
        obj_tc = self.pool.get('account.tax.code')
        ids = obj_tc.search(self.cr, self.uid, [('parent_id','=',parent)], context=context)
        res = []
        for code in obj_tc.browse(self.cr, self.uid, ids, context=context):
            res.append(('.'*2*level, code))
            res += self._get_codes(company_id, code.id, level+1, context=context)
        return res
    
    def _add_codes(self,account_list=None, period_list=None, context=None):
        if context is None:
            context = {}
            
        if account_list is None:
            account_list = []
        if period_list is None:
            period_list = []
        res = []
        obj_tc = self.pool.get('account.tax.code')
        for account in account_list:
            ids = obj_tc.search(self.cr, self.uid, [('id','=', account[1].id)], context=context)
            code = {}
            for period_ind in period_list:
                context2 = dict(context, period_id=period_ind,)
                record = obj_tc.browse(self.cr, self.uid, ids, context=context2)[0]
                if not code:
                    code = {
                        'id': record.id,
                        'name': record.name,
                        'code': record.code,
                        'sequence': record.sequence,
                        'parent_id': record.parent_id,
                    }

            res.append((account[0], code))
        return res
    
class account_vat_report(osv.AbstractModel):
    _name = 'report.l10n_ch_account_vat_payment.account_vat_reporting'
    _inherit = 'report.abstract_report'
    _template = 'l10n_ch_account_vat_payment.account_vat_reporting'
    _wrapped_report_class = account_vat_reporting

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
