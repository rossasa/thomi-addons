# -*- coding: utf-8 -*-
#/#############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2004-TODAY Tech-Receptives(<http://www.tech-receptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################

import time
from openerp.osv import osv
from openerp.report import report_sxw
from openerp import pooler
from datetime import date,datetime
from openerp import netsvc
import itertools

class account_vat_special_reporting(report_sxw.rml_parse):
    _name="account.vat.special.reporting"
    
    def __init__(self, cr, uid, name, context={}):
        super(account_vat_special_reporting, self).__init__(cr, uid, name, context=context)
        total_list = []
        self.total_list = total_list
        self.localcontext.update({
            'time': time,
            'datetime':datetime,
            'get_data':self.get_data,
            'get_start_period':self.get_start_period,
            'get_end_period':self.get_end_period,
            'get_total':self.get_total,
        }) 
    
    def get_total(self):
        return self.total_list
        
    
    def get_data(self,data,company_id=False,parent=False,level=0,context=None):
        
        res = []
        sum_lst = []
        base_sum_lst = []
        merged_lst = []
        tax_lst = []
        final_lst = []
        res_final = []
        
        dict = new_dic1 = new_dic2 = {}
        amount_total = tax_amount_total = final_total = 0.0
        sum_credit = 0.0
        sum_debit = 0.0
        sum_balance = 0.0
        sum_credit_total = 0.0
        sum_debit_total = 0.0
        cr=self.cr
        uid=self.uid
        
        inv_obj = self.pool.get('account.invoice')
        voucher_obj = self.pool.get('account.voucher')
        period_obj = self.pool.get('account.period')
        
        period_from = period_obj.browse(cr,uid,data['form']['period_from'])
        period_to = period_obj.browse(cr,uid,data['form']['period_to'])
        
        period_list = period_obj.build_ctx_periods(self.cr, self.uid, data['form']['period_from'], data['form']['period_to'])
        
        voucher_search_ids = voucher_obj.search(self.cr, self.uid, [('move_id.state','=','posted'),('move_ids.tax_code_id','!=',False),('state','=','posted'),('date', '>=',period_from.date_start),('date','<=', period_to.date_stop)],context)
        if voucher_search_ids:
            for voucher in voucher_obj.browse(cr,uid,voucher_search_ids):
                for line in voucher.move_ids:
                    if voucher.type == 'sale' or voucher.type == 'purchase':
                        move_description = line.ref or False
                    else:
                        move_description = voucher.partner_id.name or False
                    
                    if line.tax_code_id and line.base_tax_code_id:
                        if (line.name).find('/')!=-1:
                            tax_name = line.name[1:] or False
                        else:
                            tax_name = line.name
                        dict = {
                            'date':voucher.date or False,
                            'ref':voucher.move_id and voucher.move_id.name or False,
                            'move_description':move_description or False,
                            'tax_name': tax_name or False,
                            'credit': line.credit,
                            'debit': line.debit,  
                                }
                        res.append(dict)
                        final_lst.append(dict)
                        sum_credit += line.credit
                        sum_debit += line.debit
        sum_balance = abs(sum_credit - sum_debit)
        if sum_credit > sum_debit:
            sum_credit_total = sum_credit + 0.0
        else:
            sum_credit_total = sum_credit + sum_balance
        if sum_debit > sum_credit:
            sum_debit_total = sum_debit + 0.0
        else:
            sum_debit_total = sum_debit + sum_balance
            
        self.total_list.append({'sum_credit':sum_credit,'sum_debit':sum_debit,'sum_balance':abs(sum_balance),'sum_credit_total':abs(sum_credit_total),'sum_debit_total':abs(sum_debit_total)})
        
        
        if data and 'form' in data and data['form']['sorting'] and data['form']['sorting']=='date':
            if data['form']['reverse_sort'] == True:
                res_final = sorted(final_lst, key=lambda k: k['date'],reverse=True)
            else:
                 res_final = sorted(final_lst, key=lambda k: k['date'])
        elif data and 'form' in data and data['form']['sorting'] and data['form']['sorting']=='reference':
            if data['form']['reverse_sort'] == True:
                res_final = sorted(final_lst, key=lambda k: k['ref'],reverse=True)
            else:
                res_final = sorted(final_lst, key=lambda k: k['ref'])
        elif data and 'form' in data and data['form']['sorting'] and data['form']['sorting']=='description':  
             if data['form']['reverse_sort'] == True:
                 res_final = sorted(final_lst, key=lambda k: k['move_description'],reverse=True)
             else:
                 res_final = sorted(final_lst, key=lambda k: k['move_description']) 
        return res_final
            
    def get_start_period(self, data):
        if data.get('form', False) and data['form'].get('period_from', False):
            return self.pool.get('account.period').browse(self.cr,self.uid,data['form']['period_from']).name
        return ''

    def get_end_period(self, data):
        if data.get('form', False) and data['form'].get('period_to', False):
            return self.pool.get('account.period').browse(self.cr, self.uid, data['form']['period_to']).name
        return ''
    
    
    
class account_vat_report(osv.AbstractModel):
    _name = 'report.l10n_ch_account_vat_payment.account_vat_special_reporting'
    _inherit = 'report.abstract_report'
    _template = 'l10n_ch_account_vat_payment.account_vat_special_reporting'
    _wrapped_report_class = account_vat_special_reporting

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
