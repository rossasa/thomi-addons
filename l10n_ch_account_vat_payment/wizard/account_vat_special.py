# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv


class account_special_vat_wizard(osv.osv_memory):
    _inherit = 'account.vat.declaration'
    _name = 'account.special.vat.wizard'
     
    _description = 'Account Special Vat Declaration Wizard for Sale/Purchase'

    _columns = {
            'voucher_state':fields.selection([('posted','Posted only'),('all','All')],'Type'),
            'sorting':fields.selection([('date','By Date'),('reference','By Reference'),('description','By Move Description')],'Sort By'),
            'reverse_sort':fields.boolean('Reverse Sort')        
                    }
    _defaults = {
                 'voucher_state': 'posted',
                 'sorting' : 'date',
                 'reverse_sort' : False
                 }
    def print_vat(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        datas = {'ids': context.get('active_ids', [])}
        datas['model'] = 'account.tax.code'
        datas['form'] = self.read(cr, uid, ids, context=context)[0]
        
        for field in datas['form'].keys():
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]

        taxcode_obj = self.pool.get('account.tax.code')
        taxcode_id = datas['form']['chart_tax_id']
        taxcode = taxcode_obj.browse(cr, uid, [taxcode_id], context=context)[0]
        datas['form']['company_id'] = taxcode.company_id.id
        return self.pool['report'].get_action(cr, uid, [], 'l10n_ch_account_vat_payment.account_vat_special_reporting', data=datas, context=context)
         

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
