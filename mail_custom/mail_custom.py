# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    (C) Copyright 2013 Tech-Receptives Solutions (P) Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
from collections import OrderedDict
import datetime
import dateutil
import email
from email.message import Message
from email.utils import formataddr
import logging
from lxml import etree
import pytz
import re
import socket
import time
from urllib import urlencode
import xmlrpclib

from openerp import SUPERUSER_ID
from openerp import api, tools
from openerp.addons.mail.mail_message import decode
from openerp.osv import fields, osv, orm
from openerp.osv.orm import BaseModel
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools.translate import _
try:
    import simplejson as json
except ImportError:
    import json


_logger = logging.getLogger(__name__)


mail_header_msgid_re = re.compile('<[^<>]+>')


def decode_header(message, header, separator=' '):
    return separator.join(map(decode, filter(None, message.get_all(header, []))))


class mail_thread(osv.osv):
    _inherit = "mail.thread"

    def message_route(self, cr, uid, message, message_dict, model=None, thread_id=None,
                      custom_values=None, context=None):
        res = super(mail_thread, self).message_route(cr, uid, message, message_dict, model, thread_id,
                                                     custom_values, context=context)

        if not isinstance(message, Message):
            raise TypeError(
                'message must be an email.message.Message at this point')
        mail_msg_obj = self.pool['mail.message']
        mail_alias = self.pool.get('mail.alias')
        fallback_model = model

        # Get email.message.Message variables for future processing
        message_id = message.get('Message-Id')
        email_from = decode_header(message, 'From')
        email_to = decode_header(message, 'To')
        references = decode_header(message, 'References')
        in_reply_to = decode_header(message, 'In-Reply-To').strip()
        thread_references = references or in_reply_to
        localpart = (tools.email_split(email_from) or [''])[
            0].split('@', 1)[0].lower()
        if message.get_content_type() == 'multipart/report' or localpart == 'mailer-daemon':
            _logger.info("Not routing bounce email from %s to %s with Message-Id %s",
                         email_from, email_to, message_id)
            return []
        ref_match = thread_references and tools.reference_re.search(
            thread_references)
        msg_references = mail_header_msgid_re.findall(thread_references)
        mail_message_ids = mail_msg_obj.search(
            cr, uid, [('message_id', 'in', msg_references)], context=context)
        if ref_match and mail_message_ids:
            original_msg = mail_msg_obj.browse(
                cr, SUPERUSER_ID, mail_message_ids[0], context=context)
            model, thread_id = original_msg.model, original_msg.res_id
            alias_ids = mail_alias.search(cr, uid, [(
                'alias_name', '=', (tools.email_split(email_to) or [''])[0].split('@', 1)[0].lower())])
            alias = None
            if alias_ids:
                alias = mail_alias.browse(
                    cr, uid, [alias_ids[0]], context=context)
            route = self.message_route_verify(
                cr, uid, message, message_dict,
                (model, thread_id, custom_values, uid, alias),
                update_author=True, assert_model=False, create_fallback=True, context=dict(context, drop_alias=True))
            if route:
                _logger.info(
                    'Routing mail from %s to %s with Message-Id %s: direct reply to msg: model: %s, thread_id: %s, custom_values: %s, uid: %s',
                    email_from, email_to, message_id, model, thread_id, custom_values, uid)
                return [route]
            elif route is False:
                return []

        # 2. message is a reply to an existign thread (6.1 compatibility)
        if ref_match:
            reply_thread_id = int(ref_match.group(1))
            reply_model = ref_match.group(2) or fallback_model
            reply_hostname = ref_match.group(3)
            local_hostname = socket.gethostname()
            # do not match forwarded emails from another OpenERP system
            # (thread_id collision!)
            if local_hostname == reply_hostname:
                thread_id, model = reply_thread_id, reply_model
                if thread_id and model in self.pool:
                    model_obj = self.pool[model]
                    compat_mail_msg_ids = mail_msg_obj.search(
                        cr, uid, [
                            ('message_id', '=', False),
                            ('model', '=', model),
                            ('res_id', '=', thread_id),
                        ], context=context)
                    if compat_mail_msg_ids and model_obj.exists(cr, uid, thread_id) and hasattr(model_obj, 'message_update'):
                        route = self.message_route_verify(
                            cr, uid, message, message_dict,
                            (model, thread_id, custom_values, uid, None),
                            update_author=True, assert_model=True, create_fallback=True, context=context)
                        if route:
                            # parent is invalid for a compat-reply
                            message_dict.pop('parent_id', None)
                            _logger.info(
                                'Routing mail from %s to %s with Message-Id %s: direct thread reply (compat-mode) to model: %s, thread_id: %s, custom_values: %s, uid: %s',
                                email_from, email_to, message_id, model, thread_id, custom_values, uid)
                            return [route]
                        elif route is False:
                            return []

        # 3. Reply to a private message
        if in_reply_to:
            mail_message_ids = mail_msg_obj.search(cr, uid, [
                ('message_id', '=', in_reply_to),
                '!', ('message_id', 'ilike', 'reply_to')
            ], limit=1, context=context)
            if mail_message_ids:
                mail_message = mail_msg_obj.browse(
                    cr, uid, mail_message_ids[0], context=context)
                route = self.message_route_verify(cr, uid, message, message_dict,
                                                  (mail_message.model, mail_message.res_id,
                                                   custom_values, uid, None),
                                                  update_author=True, assert_model=True, create_fallback=True, allow_private=True, context=context)
                if route:
                    _logger.info(
                        'Routing mail from %s to %s with Message-Id %s: direct reply to a private message: %s, custom_values: %s, uid: %s',
                        email_from, email_to, message_id, mail_message.id, custom_values, uid)
                    return [route]
                elif route is False:
                    return []
        message_dict.pop('parent_id', None)
        rcpt_tos = \
            ','.join([decode_header(message, 'Delivered-To'),
                      decode_header(message, 'To'),
                      decode_header(message, 'Cc'),
                      decode_header(message, 'Resent-To'),
                      decode_header(message, 'Resent-Cc')])
        local_parts = [e.split('@')[0] for e in tools.email_split(rcpt_tos)]

        if local_parts:
            author_email = [
                e.split('>')[0] for e in tools.email_split(message_dict['from'])][0]
            partner_ids = self.pool.get('res.partner').search(
                cr, uid, [('email', '=', author_email)], context=context)
            alias_ids = mail_alias.search(
                cr, uid, [('alias_name', 'in', local_parts)])
            if alias_ids:
                partner_routes = []
                routes = []
                partner_follower = []
                for alias in mail_alias.browse(cr, uid, alias_ids, context=context):
                    user_id = alias.alias_user_id.id
                    if not user_id:
                        user_id = uid
                        _logger.info(
                            'No matching user_id for the alias %s', alias.alias_name)
                    if alias.runtime_alias == True and partner_ids:
                        for partner in self.pool.get('res.partner').browse(cr, uid, partner_ids, context=context):
                            for follower in partner.message_follower_ids:
                                partner_follower.append(follower[
                                    0].id)
                            user_partner_id = self.pool.get('res.users').browse(
                                cr, uid, alias.alias_force_thread_id, context=context).partner_id.id
                            if user_partner_id in partner_follower:
                                self.pool.get('mail.alias').write(cr, uid, alias_ids[0], {
                                    'partner_force_thread_id': partner.id}, context=context)

                                route = ('res.partner', alias.partner_force_thread_id, eval(
                                    alias.alias_defaults), user_id, alias)
                                route = self.message_route_verify(cr, uid, message, message_dict, route,
                                                                  update_author=True, assert_model=True, create_fallback=True, context=context)
                                if route:
                                    _logger.info(
                                        'Routing mail from %s to %s with Message-Id %s: direct alias match: %r',
                                        email_from, email_to, message_id, route)
                                    partner_routes.append(route)
                            else:
                                self.pool.get('mail.alias').write(cr, uid, alias_ids[0], {
                                    'partner_force_thread_id': partner.id}, context=context)

                                route = ('res.partner', alias.partner_force_thread_id, eval(
                                    alias.alias_defaults), user_id, alias)
                                route = self.message_route_verify(cr, uid, message, message_dict, route,
                                                                  update_author=True, assert_model=True, create_fallback=True, context=context)
                                if route:
                                    _logger.info(
                                        'Routing mail from %s to %s with Message-Id %s: direct alias match: %r',
                                        email_from, email_to, message_id, route)
                                    partner_routes.append(route)
                                    if res[0] not in partner_routes:
                                        partner_routes.append(res[0])
                        return partner_routes
                return res

        # 5. Fallback to the provided parameters, if they work
        if not thread_id:
            # Legacy: fallback to matching [ID] in the Subject
            match = tools.res_re.search(decode_header(message, 'Subject'))
            thread_id = match and match.group(1)
            # Convert into int (bug spotted in 7.0 because of str)
            try:
                thread_id = int(thread_id)
            except:
                thread_id = False
        route = self.message_route_verify(cr, uid, message, message_dict,
                                          (fallback_model, thread_id,
                                           custom_values, uid, None),
                                          update_author=True, assert_model=True, context=context)
        if route:
            _logger.info(
                'Routing mail from %s to %s with Message-Id %s: fallback to model:%s, thread_id:%s, custom_values:%s, uid:%s',
                email_from, email_to, message_id, fallback_model, thread_id, custom_values, uid)
            return [route]

        # ValueError if no routes found and if no bounce occured
        raise ValueError(
            'No possible route found for incoming message from %s to %s (Message-Id %s:). '
            'Create an appropriate mail.alias or force the destination model.' %
            (email_from, email_to, message_id)
        )


class mail_alias(osv.Model):
    _inherit = "mail.alias"
    _columns = {

        'runtime_alias': fields.boolean(string="Runtime Partner Record Thread Id"),
        'partner_force_thread_id': fields.integer('Partner Thread ID'),
    }

 # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
